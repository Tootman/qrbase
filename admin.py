from equipdb.models import  Item,  History
from django.contrib import admin

#admin.site.register(Book)
admin.site.register(Item)
admin.site.register(History)
