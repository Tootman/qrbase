from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, render
from django.template import  RequestContext
from django.contrib import auth
from django.core.context_processors import csrf
from  equipdb.models import Item, History, Category, Category_lookup
from django.contrib.auth.forms import UserCreationForm
from django.db.models import Q
from django.contrib.auth.models import User
import datetime
import pyqrcode
from io import StringIO
from django import forms
import logging
from django.conf import settings
from PIL import Image
from django.contrib.auth.decorators import login_required

import subprocess
#from django.templatetags.static import static

# the Cairo module in qrlabel is not available in the dev environment
try:
    from qrlabel import Qrlabel
except ImportError:
    pass


class MyClass:
    """A simple example class"""
    i = 12345
    def f(self):
        return 'hello world'

class UploadFileForm(forms.Form):
    metitle = forms.CharField(max_length=50)
    mefile  = forms.FileField()


#def home (request):
#    return HttpResponse("Hello from home")

# this sends a literal string to the browser
def about (request):
    # return HttpResponse("Sorry not done yet")
    return render_to_response("equipdb/about.html")


# this sends variables in the dictionary (param2) to template (param1) , which is output to the browser 
def home(request):
	
	all_categories = Category.objects.all()

	return render_to_response("equipdb/home.html", {
		'categories':all_categories,
		'myVar':"Dan"},context_instance=RequestContext(request))

# add a catagory to an Item
@login_required(login_url='/login/')
def add_category(request):
	item_id = request.POST['item_id']
	cat_id = request.POST['cat_id']
	my_item = Item.objects.get(id=item_id)
	my_cat = Category.objects.get(id=cat_id)
 
	# note that you pass a field object, not a fk int
	new_category = Category_lookup(
		#item=item_id,
		#category = cat_id)
		item = my_item,
		category = my_cat 
	)
	new_category.save()
	return HttpResponseRedirect('/' + item_id)


	# remove a catagory to an Item
@login_required(login_url='/login/')
def remove_category(request):
	item_id = request.POST['item_id']
	cat_id = request.POST['cat_id']
	my_record = Category_lookup.objects.get(item=item_id, category=cat_id)
	my_record.delete()
	return HttpResponseRedirect('/' + item_id)


def filter_category(request):
	cat_id = request.GET['category']
	#my_record = Category_lookup.objects.get(item=item_id, category=cat_id)
	#my_record.delete()
	try:
		my_cat = int(cat_id)
	except:
		my_id =0 # defualt if string want made of digits

	catname = Category.objects.get(id=my_cat).name
	my_rs = Item.objects.filter(category_lookup__category = my_cat).order_by('id')
	return render_to_response( "equipdb/search_grid.html",{'my_rs':my_rs,
		'heading':"Category: " + catname
		}, context_instance=RequestContext(request))


def display_item(request,item_id ):

	# create instance of history (comments/content) record set
	my_comments =  History.objects.filter (item = item_id)
	my_user_list = list(User.objects.all())
	my_children = Item.objects.filter(container= item_id) # container is foreign item id key
	try:
		my_item = Item.objects.get(id=item_id)
	except:
		 return render_to_response("equipdb/item_missing.html", context_instance=RequestContext(request))
	else:
		try:
			# retrieve parent container
			my_parent_description = Item.objects.get(id=my_item.container).descriptive_name

		except:
			my_parent_description = "None - no parent container"
		
		# category table  is effectively already joined to catagory_lookup table (via category_lookup'catagory' field, through the fk definition in the models
		# so just need to look up the categories matching this item
		my_categories = Category.objects.filter(category_lookup__item = my_item)
		other_categories = Category.objects.exclude(category_lookup__item = my_item) # all other cats
		if my_item.on_loan_to_user >0:  # come back to this workaround -replace with constant
			loan_to_field = User.objects.get(id=my_item.on_loan_to_user).username
		else:
			loan_to_field = 'This item is not currently on loan, and can be found be found in LittleHouse'
		return render_to_response("equipdb/display_item.html", {
			'id': item_id,
			'descriptive_name':my_item.descriptive_name,
			'model_txt':my_item.model_txt,
			'serialno':my_item.serialno,
			'notes':my_item.notes,
			#'user':my_item.user
			'user': User.objects.get(id=my_item.user).username,
			'my_comments': my_comments,
			#'on_loan_to_user':my_item.on_loan_to_user,
			'on_loan_to_user':loan_to_field,
			'container':my_item.container,
			'user_list':my_user_list,
			'children_list':my_children,
			'parent_description':my_parent_description,
			'categories': my_categories,
			'other_categories': other_categories
			},
			context_instance=RequestContext(request))


def new_item_return(request):
	
	# condition is not working!
	if 'item_descr_name_txt' in request.POST:
		#new_item = Item(descriptive_name='Fred Flintstone- myitem1')

		# create instance of Item  object (which maps to a new row in Items table )
		new_item = Item(descriptive_name=request.POST['item_descr_name_txt'],
			model_txt = request.POST['item_model_txt'],
			serialno = request.POST['item_serialno_txt'],
			notes = request.POST['item_notes_txt'],
			user = request.user.id,
			on_loan_to_user = 0, # default to 0 which will be 'little House'
			container = 0) # default to 0, which is Little House

		new_item.save()  #
		#create_label(new_item.id)
		return HttpResponseRedirect('/'+ str(new_item.id))


		#p.shirt_size
		#message = 'You have just created a new item with name: %s and description %s' % (request.POST['item_name_txt'], request.POST['item_description_txt'] )
		#return render_to_response("equipdb/new_item_return.html", { 'my_id': new_item.id, 
		#	'my_name':new_item.descriptive_name, 
		#	'my_my_model':new_item.model_txt,
		#	'my_serialno':new_item.serialno,
		#	'my_notes': new_item.notes
		#	},
		#	context_instance=RequestContext(request))
		
	else:
		message = 'You submitted an empty form - go back and  try again  :-('
	return HttpResponse(message)


def search (request):
	my_search = request.POST['search_str']
	try:
		my_id = int(my_search)
	except:
		my_id =0 # defualt if string want made of digits

	# this isnt ideal - if my_string consists of digits only then the id__exact match should appear at top of list
	# so the 'icontains' recordset should be appended to the id__exaxt recordset
	my_rs = Item.objects.filter (Q(descriptive_name__icontains = my_search) | Q(id__exact = my_id))
	#return render_to_response( "equipdb/search.html",{'my_rs':my_rs}, context_instance=RequestContext(request))
	return render_to_response( "equipdb/search_grid.html",{
		'my_rs':my_rs,
		'heading': "Search: " + my_search
		}, 
		context_instance=RequestContext(request))

@login_required(login_url='/login/')
def new_comment(request):
	# issues with terminology
	my_item_id = request.POST['item_id']
	new_note = History(content=request.POST['new_note'],
	 item_id = int(my_item_id),
	 #user = request.user.username,
	 user_id = request.user.id,
	 pub_date = datetime.datetime.now()
	 )
	new_note.save()
	
	#HttpResponse("done")
	#return HttpResponseRedirect('/home')
	return HttpResponseRedirect('/'+ my_item_id)


def mytest (request):
	if request.user.is_authenticated():

		var1= request.user.username
	else:
		var1= "logged out"

	return render_to_response( "equipdb/test.html",{'var1':var1},  context_instance=RequestContext(request))
	#return render_to_response( "equipdb/test.html",{'var1':var1})


def login(request):
	# login created following youtube vid: Django tutorial 9 - users login and logout by Mike Hibbert 
	c = {}
	c.update(csrf(request))
	return render_to_response("equipdb/login.html", c)


def auth_view (request):
	#return HttpResponse("hello fro auth")
	username = request.POST.get('username','')
	password = request.POST.get('password','')
	user = auth.authenticate(username=username, password = password)
	if user is not None:
		auth.login(request, user)
		return HttpResponseRedirect('/equipdb/loggedin.html')
	else:
		return HttpResponseRedirect('/equipdb/invalid_user.html')


def loggedin (request):
	return render_to_response('equipdb/loggedin.html')


def logout(request):
	auth.logout(request)
	return render_to_response('equipdb/logout.html',)
	

def invalid_user(request):
	auth.logout(request)
	return render_to_response('equipdb/invalid_user.html',)


def register_user(request):
	if request.method=='POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/accounts/register_success')
	args={}
	args.update(csrf(request))
	args['form'] = UserCreationForm()
	return render_to_response('equipdb/register.html',args)


def register_success(request):
	return render_to_response('equipdb/register_success.html')


@login_required(login_url='/login/')
def upload(request):

	#myuploadedfile = request.FILES[forms.FileField()]
	a = MyClass()
	b = "hello" + str(a.i)
	#logging.warning(" value of i is", a.i) # will print a message to the console
	logging.warning(b)
	item_id = request.POST['item_id']
	form = UploadFileForm(request.POST, request.FILES) 
	handle_uploaded_file( request.FILES['mefile'], item_id)
	#return HttpResponse('image upload success')
	return HttpResponseRedirect('/' + item_id)

def reprint_qr(request):
	
		# prints the current item
		#item_id = request.POST['item_id']
		#myqr_inst = Qrlabel(item_id)
		#myqr_inst.create_label()
		#return render_to_response("equipdb/display_qr.html", { 'my_id': item_id},
			#context_instance=RequestContext(request))

		# prints the whole lot - this is a work around 
		for id in range(62,80):
			myqr_inst = Qrlabel(id)
			myqr_inst.create_label()
		return HttpResponse("done")






@login_required(login_url='/login/')
def change_loan (request):
	# this function handles all thanges in the item page not just change loan
	item_id = request.POST['item_id']
	my_item = Item.objects.get(id=item_id)
	my_item.on_loan_to_user = request.POST['change_loan_id']
	my_item.model_txt = request.POST['model_txt']
	my_item.serialno = request.POST['serialno']
	my_item.notes = request.POST['notes']
	my_container_id = request.POST['container']
	if my_container_id != 'None':
		my_item.container = my_container_id
	else:
		my_item.container= None;
	my_item.save()
	return HttpResponseRedirect('/' + item_id)


@login_required(login_url='/login/')
def print_label (request):
	 logging.warning("print label called")

	 #return HttpResponse("You should see your label print out - if not check that it is connected etc")
	 #subprocess.Popen(['lpr', 'my_label.svg'])
	 #my_status = subprocess.check_output(["lpstat -p Labelwriter"])
	 #my_status = os.system("lpstat -p Labelwriter")
	 #my_message = "OK. You should see your label print out - if not, please check that it is connected etc"
	 #my_message = my_status
	 

	 #my_message = subprocess.check_output(['lpstat', '-p', 'Labelwriter'])
	 try:
	 	my_message =subprocess.check_output(['lpstat', '-p', 'Labelwriter'])
	 except:
	 	my_message = "this function doesnt work here- you are probaby on the windows dev version." 

	 return render_to_response("equipdb/message.html", { 'message': my_message,  'action_page':'home','call_to_action':'Back to Home page'},
			context_instance=RequestContext(request))


# ---------------------------- stuff that should be seperate class / file -------------------------


def handle_uploaded_file(f, item_id):

    # location includes file name
    UPLOADED_IMAGE_NAME = 'uploadedfile.jpg'
    #mypath = str(settings.UPLOADED_IMAGE_LOCATION + '\\' + UPLOADED_IMAGE_NAME)
    mypath = str(settings.UPLOADED_IMAGE_LOCATION + '.jpg') # path inc base filename
    destination = open(mypath, 'wb+') # # location includes file name
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()
    logging.warning("mypath= " + mypath)
    # generate thumbnail
    thumb_size = (256,256) # 
    #im = Image.open(settings.UPLOADED_IMAGE_LOCATION)
    #im = Image.open(settings.UPLOADED_IMAGE_LOCATION + '\\' + UPLOADED_IMAGE_NAME)
    im = Image.open(settings.UPLOADED_IMAGE_LOCATION + '.jpg')
    im = im.resize((256,256), Image.ANTIALIAS)
    #im.save(settings.UPLOADED_IMAGE_LOCATION + '\\' + 'thumbnail.jpg')
    #im.save(settings.UPLOADED_IMAGE_LOCATION + '\\' + 'thumbnail_'+ item_id + '.jpg')
    im.save(settings.UPLOADED_IMAGE_LOCATION +'_thumbnail_' +  item_id + '.jpg')
   

