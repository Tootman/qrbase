from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


#class Question(models.Model):
#	question_text = models.CharField(max_length=200)
#	pub_date = models.DateTimeField('date published')


#class Choice(models.Model):
#	question = models.ForeignKey(Question)
#	choice_text = models.CharField(max_length=200)
#	votes = models.IntegerField(default=0)


#class Article(models.Model):
#	title = models.CharField(max_length=200)
#	body = models.TextField()
#	pub_date = models.DateTimeField('date published')
#	likes = models.IntegerField()

#	def __unicode__(self):
#		return self.title

#class Book (models.Model):
#	title= models.CharField(max_length=150)
#	author_name= models.CharField(max_length=150)
#	def __unicode__(self):
#		return self.title + " / " + self.author

class Item(models.Model):
	descriptive_name = models.CharField(max_length=100)
	model_txt = models.CharField(max_length=100)
	serialno = models.CharField(max_length=50)
	notes = models.TextField()
	user = models.IntegerField() # use directly from auth session logged in user
	#on_loan_to_user = models.ForeignKey(User) # look up from auth users - doesnt work - why??
	on_loan_to_user = models.IntegerField(null=True, blank=True) # just for testing - this works
	container= models.IntegerField() # if not null, contains the item_id of the container housing the item

	def __unicode__(self):
		return self.descriptive_name


class History(models.Model):
	#content = models.TextField()
	item = models.ForeignKey(Item)
	content = models.TextField()
	user = models.ForeignKey(User)# looked up  directly from auth table
	pub_date =models.DateTimeField()
	def __unicode__(self):
		return self.content

class Category (models.Model):
	name = models.CharField(max_length=20)


class Category_lookup (models.Model):
	item = models.ForeignKey(Item)
	category = models.ForeignKey(Category)
	#category = models.ManyToManyField(Category)
	#item = models.ManyToManyField(Item)