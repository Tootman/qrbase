from PIL import Image
import pyqrcode
from io import StringIO
import logging
from django.conf import settings
import cairosvg
from PIL import Image


class Qrlabel(object):
    """A simple example class"""

    i = 12345
    def f(self):
      return 'hello world'

    def __init__(self, my_id):
        self.item_id = my_id

    def testfunc(self):
      return self.item_id


    def create_label(self):

      # this creates a svg file file by concatinating svg_head + my_qrcode.scv + sgv_mid_section + url + svg_last_section 
      # and outputs it to a text file called mylabel.svg
      # create and return a qrcode object 
      
      logging.warning("create label called")
      myUrl = r'kit.richmondmakerlabs.uk/' + str(self.item_id)
      #urlQR = pyqrcode.create(myUrl)
      svg_head= """<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- Created with Inkscape (http://www.inkscape.org/) -->

<svg
   xmlns:dc="http://purl.org/dc/elements/1.1/"
   xmlns:cc="http://creativecommons.org/ns#"
   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
   xmlns:svg="http://www.w3.org/2000/svg"
   xmlns="http://www.w3.org/2000/svg"
   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"
   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"
   width="180.70866"
   height="67.32283"
   id="svg2"
   version="1.1"
   inkscape:version="0.48.5 r10040"
   sodipodi:docname="qr-code-test1 - orig size-get.svg">
  <defs
     id="defs4" />
  <sodipodi:namedview
     id="base"
     pagecolor="#ffffff"
     bordercolor="#666666"
     borderopacity="1.0"
     inkscape:pageopacity="0.0"
     inkscape:pageshadow="2"
     inkscape:zoom="1.979899"
     inkscape:cx="264.64286"
     inkscape:cy="41.486455"
     inkscape:document-units="px"
     inkscape:current-layer="layer1"
     showgrid="false"
     inkscape:window-width="1366"
     inkscape:window-height="746"
     inkscape:window-x="-8"
     inkscape:window-y="760"
     inkscape:window-maximized="1"
     units="mm" />
  <metadata
     id="metadata7">
  
  </metadata>
  <g
     inkscape:label="Layer 1"
     inkscape:groupmode="layer"
     id="layer1"
     transform="translate(0,-985.03935)">
    <g
       id="my_id"
       transform="matrix(0.22,0,0,0.22,116,985)"
       inkscape:label="#g3002">
      <rect
         id="rect2991"
         height="280"
         width="280"
         y="0"
         x="0"
         style="fill:#ffffff;fill-opacity:1" />
      <g
         id="my_qr_element">"""


      svg_mid_section = """</g>
    </g>
    <text
       xml:space="preserve"
       style="font-size:10px;font-style:normal;font-weight:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Sans"
       x="18"
       y="1030.3121"
       
       sodipodi:linespacing="125%"
       transform="scale(0.98491165,1.0153195)"><tspan
         sodipodi:role="line"
         
         x="20"
         y="1030"
         style="font-size:7px" id="my_url"> """
  
      svg_section3 = """</tspan></text>
    <text
       xml:space="preserve"
       style="font-size:13.9713583px;font-style:normal;font-variant:normal;font-weight:bold;font-stretch:normal;line-height:125%;letter-spacing:0px;word-spacing:0px;fill:#000000;fill-opacity:1;stroke:none;font-family:Segoe UI;-inkscape-font-specification:Segoe UI Bold"
       x="0"
       y="958.6004"
       id="text3011"
       sodipodi:linespacing="125%"
       transform="scale(0.92790588,1.0776955)"><tspan
         sodipodi:role="line"
         
         x="20"
         y="957"
         style="font-size:42px;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-family:Segoe UI;-inkscape-font-specification:Segoe UI Bold" id="rml_label">"""


      svg_section4 = """</tspan></text>
  </g>
</svg> """
      my_qr = pyqrcode.create(myUrl)
      pass
      file_str = StringIO()
      my_qr.svg(file_str,scale=8)
      qr_str = file_str.getvalue()
      #qrfile = open(r'D:\documents\pythonApps\2-7_apps\djs-django\equipdb\equipdb\static\\my_qrcode.svg'
      logging.warning("myUrl: ")
      logging.warning(myUrl)
      logging.warning(settings.SVG_FILE_LOCATION)
      label_file_str= svg_head + qr_str + svg_mid_section + myUrl + svg_section3 +  format(self.item_id, '04') + svg_section4
       #label_file  = open(r'D:\documents\pythonApps\2-7_apps\djs-django\equipdb\equipdb\static\\my_label.svg','w')
      #label_file  = open(r'equipdb\static\\my_label.svg','w')
      
      label_file  = open(settings.SVG_FILE_LOCATION,'w')
      
      #label_file.write(label_file_str)
      # ------------------------------ comnverting from svg to png -----------------------
      # tried: sudo pip install cairosvg
      # sudo apt-get install libcairo2-dev
      # Tried to use cairosvg  but not finding cairo package 
      # NOt conversion of QR code is not right
      
      cairosvg.svg2png(bytestring=label_file_str, write_to=label_file, dpi=300)
      cairosvg.svg2ps (bytestring=label_file_str, write_to='/home/dan/python-apps/django-test/equipdb/equipdb/static/images/my_label_' + str(self.item_id) +'.ps')
      label_file.close()


      #  ----- djs ------
      final_label = Image.open(settings.SVG_FILE_LOCATION)
      final_label = final_label.resize((360,136)) # or is it 192px x 72px
      final_label.save('/home/dan/python-apps/django-test/equipdb/equipdb/static/images/my_label-small.png')


      
      
      
      # ------------------------------------------------------------------------------------
      #urlQR.svg (r'D:\documents\pythonApps\2-7_apps\djs-django\equipdb\equipdb\static\\my_qrcode.svg', scale=7) 
      #return urlQR.svg (r'D:\documents\pythonApps\2-7_apps\djs-django\equipdb\equipdb\static\\my_qrcode2.svg', scale=8) 