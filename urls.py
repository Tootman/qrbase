from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.contrib.auth.decorators import login_required
#from django.views.generic.simple import direct_to_template # now deprecated
from django.views.generic import TemplateView

admin.autodiscover()

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
     url(r'^$', 'equipdb.views.home', name='home'),
     url(r'^home/$', 'equipdb.views.home', name='home'),
     url(r'^about', 'equipdb.views.about', name='about'),
     #url(r'^kit.richmond.ml/', 'equipdb.views.display_item', name='display_item'),
     #url(r'^(\d{6})', 'equipdb.views.display_item', name='display_item'),
     url(r'^(\d+$)', 'equipdb.views.display_item', name='display_item'),
     #url(r'^new_item_form', 'equipdb.views.new_item_form', name='new_item_form'),
     url(r'^new_item_form', login_required(TemplateView.as_view(template_name='equipdb/new_item_form.html'))),
     url(r'^new_item_return/$', 'equipdb.views.new_item_return'),
     url(r'^search/', 'equipdb.views.search'),
     url(r'^new_comment/', 'equipdb.views.new_comment'),
     url(r'^login/', 'equipdb.views.login'),
     url(r'^accounts/login/', 'equipdb.views.login'),
     url(r'^logout/', 'equipdb.views.logout'),
     url(r'^auth', 'equipdb.views.auth_view'),
     url(r'loggedin', 'equipdb.views.loggedin'),
     url(r'invalid_user', 'equipdb.views.invalid_user'),
     url(r'^mytest/', 'equipdb.views.mytest'),
     #url(r'^upload/', 'equipdb.views.upload'),
     url(r'^upload/', 'equipdb.views.upload'),
     url(r'^accounts/register/$', 'equipdb.views.register_user'),
     url(r'^accounts/register_success', 'equipdb.views.register_success'),
     url(r'^reprint_qr', 'equipdb.views.reprint_qr'),
     url(r'^change_loan', 'equipdb.views.change_loan'),
     url(r'^print_label', 'equipdb.views.print_label'),
     url(r'^add_category', 'equipdb.views.add_category'),
     url(r'^remove_category', 'equipdb.views.remove_category'),
     url(r'^cat', 'equipdb.views.filter_category'),


        
        
        

    # url(r'^equipdb/', include('equipdb.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
     url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
