# QRBase #
QRBase is an online tool intended for members of [RichmondMakerLabs.uk](http://richmondmakerlabs.uk "richmondmakerlabs.uk") ('RML') DIY Tech club, for tracking and managing RML equipment, and container contents.
  
QRBase applies to following types of item:

- DIY Tech projects, or components/modules of projects
- Workshop Tools and electronic test equipment
- Donated equipment
- Containers (ie cupboards, drawers and boxes)

## QRBase features ##

- Label with QR code automatically generated and printed out (then affixed to equipment)
- An item can be a 'child' of another item (the parent item could be a container, or another project)
- Mobile first UI design - using Bootstrap, with flexible grid layout
- CMS based architecture  - ie registered users can add timestamped comments and notes, upload a featured image of the item (to help identification), filter by categories and tags etc




## Version ##
CURRENT VERSION: v0.9 (BETA)

PLEASE NOTE THIS APPLICATION IS STILL IN DEVELOPMENT



## Who do I talk to? ##

Software: Dan

Server configuration: Ian B

## About ##
This app is written in Python 2, and uses the Django framework. THe WebApp is hosted on a Debian headless server, on the RichmondMakerLabs premises, in Ham Close.

## How it works ##

### Process ###
- The user creates a new database entry for a new item of equipment (mySQL backend). 
- The user can also upload a photo of the item, or add timestamped comments, or add further details to this item.

#### QR label generation ####
a SVG format template for the label was generated using Inkskape. The item id, url and QRcode svg data is injected into the SVG file at the appropriate places. The QRCode file is generated using PyQRcode.


#### Example label ####
>![example-label.jpg](https://bitbucket.org/repo/GdqKrR/images/4172146036-example-label.jpg)


## Usage ##

### General notes###
Ideally you need to use a smartphone with a reasonably good camera, and that has a QRreader app installed. This is needed to read the QRLalel, and for uploading a photo of an item, if you are createing an entry for a new item.

### How to view details for an item ###

Use a smartphone with a QR reader app, to read  the QR label attached to the item. The QR code links to the item's page on the AQBase WebApp.

- If you know the id number of the item then the page can be access in a browser using:
kit.richmondmakerlabs.uk/n where n is the id number. (eg [kit.richmondmakerlabs.uk/43](kit.richmondmakerlabs.uk/43 "kit.richmondmakerlabs.uk/43"))

- Alternatively you can search for an item using the search bar, or browse an item by clicking search, leaving the search text blank.

### How to create a new item ###

1. log in (create a new account if necessary)
1. Select 'New item'
1. Fill in fields as necessary and submit
1. Click change featured image to allow your phone's camera to take a picture of the item. The select upload to set the photo as the 'featured' image for the item
1. Add categories to item as necessary 
1. Add Any any comments as necessary -

** NOTE **
Please put a TEMPORARY sticker on the item to indicate that the item has already been added to the database (Any old sticker/label ). On the sticker please write the ID number of the item

## Known bugs ##
- The Printer driver (using 'CUPS') for the Dymo label printer does not seem to be working with the Debian Server - so labels can not be printed in-situ. Labels are therefore currently printed from another computer that is not accessable from Little house.
**See IanB or Dan, if you need labels printed out.**. In the meantime, after you have created a entry for a new item, please mark the item with the newly created ID number, using a temporary label.


## Features and enhancements Todo ##
- Online Booking system with calendar to allow users to reserve an item for use, and to request permission from the item owner (if applicable), to do so.
- Impliment pagination for limiting the number of items viewed on page, when serching and browsing for items.
- Design and impliment test plan, test cases unit testing
- Refactoring code: move all inline CSS to a CSS file.
 - Complete this Readme, adding diagrams and images of usage workflow and example label.
- Add release history /version documentation, to this readme
- Add credits for contributors to this project
- Program flow immediately after user login or Registering not yet sorted out